module.exports = {
  "projects_names": {
    "Do-er": {
      logo: 'https://firebasestorage.googleapis.com/v0/b/robusta-secrets-management.appspot.com/o/logos%2Fdo-er.png?alt=media&token=e5002d67-4980-4f09-961b-555d9ebcda80',
    },
    "Farmraiser": {
      logo: 'https://firebasestorage.googleapis.com/v0/b/robusta-secrets-management.appspot.com/o/logos%2Ffarmraiser.png?alt=media&token=859ddbd0-f6c4-490a-b2a5-464568205e19'
    },
    "Filkhedma": {
      logo: 'https://firebasestorage.googleapis.com/v0/b/robusta-secrets-management.appspot.com/o/logos%2Ffilkhedma.png?alt=media&token=0d5eed69-8bdb-4946-9b60-3e5d737cd2d3'
    },
    "GS1": {
      logo: 'https://firebasestorage.googleapis.com/v0/b/robusta-secrets-management.appspot.com/o/logos%2Fgs1.svg?alt=media&token=2a56c017-01b1-4dea-945a-a98422c68015'
    },
    "Golden Stores": {
      logo: ''
    },
    "InternsGoPro": {
      logo: 'https://firebasestorage.googleapis.com/v0/b/robusta-secrets-management.appspot.com/o/logos%2Finternsgopro.png?alt=media&token=775470e1-d1dd-4b15-bd95-c20bf03fb0d4'
    },
    "Journey": {
      logo: 'https://firebasestorage.googleapis.com/v0/b/robusta-secrets-management.appspot.com/o/logos%2Fjourney.png?alt=media&token=aa7dd54f-8a2b-4b29-96a3-810d958978bb'
    },
    "Khoshala": {
      logo: ''
    },
    "Limopedia": {
      logo: ''
    },
    "MallBuddy": {
      logo: 'https://firebasestorage.googleapis.com/v0/b/robusta-secrets-management.appspot.com/o/logos%2Fmallbuddy.png?alt=media&token=aa4102e3-1e02-406e-b866-1956bbb2007f'
    },
    "Mirqah": {
      logo: ''
    },
    "P&G": {
      logo: ''
    },
    "PassApp": {
      logo: 'https://firebasestorage.googleapis.com/v0/b/robusta-secrets-management.appspot.com/o/logos%2Fpassapp.png?alt=media&token=d4220b4e-4e85-402f-8362-8fb4e0135281'
    },
    "Peeps": {
      logo: ''
    },
    "Pinmelocal": {
      logo: 'https://firebasestorage.googleapis.com/v0/b/robusta-secrets-management.appspot.com/o/logos%2Fpinmelocal.png?alt=media&token=4c4b33b0-6c55-47e6-9d67-c1362d45c090'
    },
    "Rails Staging": {
      logo: 'https://firebasestorage.googleapis.com/v0/b/robusta-secrets-management.appspot.com/o/logos%2Frobusta.png?alt=media&token=32ab8b54-4d5f-46ac-a02f-d9689af85a39'
    },
    "RecForFree": {
      logo: ''
    },
    "Robusta Sharing Centre": {
      logo: 'https://firebasestorage.googleapis.com/v0/b/robusta-secrets-management.appspot.com/o/logos%2Frobusta.png?alt=media&token=32ab8b54-4d5f-46ac-a02f-d9689af85a39'
    },
    "SeaShell": {
      logo: 'https://firebasestorage.googleapis.com/v0/b/robusta-secrets-management.appspot.com/o/logos%2Fseashell.png?alt=media&token=abe9a390-d07d-4c0e-8de9-04606c255ad8'
    },
    "Robusta Services": {
      logo: 'https://firebasestorage.googleapis.com/v0/b/robusta-secrets-management.appspot.com/o/logos%2Frobusta.png?alt=media&token=32ab8b54-4d5f-46ac-a02f-d9689af85a39'
    },
    "Snap Grad": {
      logo: 'https://firebasestorage.googleapis.com/v0/b/robusta-secrets-management.appspot.com/o/logos%2Fsnap-grad.png?alt=media&token=8b79970d-9819-4bd4-92e3-b2e7a6217a40'
    }
  },
  "projects" : {
    "Do-er" : {
      "sections" : {
        "AWS" : {
          "Password" : "v%$6PbTSMVRp",
          "URL" : "https://doer.signin.aws.amazon.com/console",
          "Username" : "Robusta"
        },
        "Access Keys" : {
          "Key ID" : "AKIAJJCLKNVJTICEOIZA",
          "Key Secret" : "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"
        },
        "Apple ID " : {
          "pass" : "7EmFIQs9",
          "username" : "m.zohair.ammar@gmail.com "
        },
        "Production Server  neo4j" : {
          "password" : "3854QL$d8T5x",
          "user" : "neo4j"
        },
        "Production Server database" : {
          "deployer pass" : "Ae2371$$Bl9a",
          "deployer user" : "deployer",
          "root pass" : "Ae2371$$Bl9a",
          "root user" : "root"
        },
        "Production Server linode" : {
          "command" : "ssh -p 6313 deployer@139.162.172.237",
          "email" : "m.zohair.ammar@gmail.com",
          "password" : "Ultu521W",
          "root password" : "Doer@123",
          "username" : "zohair"
        },
        "Staging Server Access" : {
          "password" : "doerneo4j",
          "url" : "http://139.162.178.120:7000/browser/",
          "user" : "neo4j"
        },
        "Staging linode Account" : {
          "password" : "Doerstaging",
          "root password" : "Doer@123",
          "username" : "robustadoer"
        },
        "eurodns (SSL certificate only) " : {
          "email" : "m@eldib.com ",
          "password" : "bz646hn5S",
          "username" : "doer "
        },
        "hover (Domain URL)" : {
          "password" : "P319wL6A",
          "username" : "Doer"
        },
        "mailgun (email service provider) " : {
          "pass" : "Hvj1kLXz22tc",
          "username" : "m.zohair.ammar@gmail.com"
        }
      }
    },
    "Farmraiser" : {
      "sections" : {
        "Blog" : {
          "Admin username" : "farmraiser_admin",
          "password" : "orV!VBNMma6@"
        },
        "Digital ocean (production server)" : {
          "URL" : "https://cloud.digitalocean.com/login",
          "email" : "mark@farmraiser.com",
          "password" : "FarmServe123$55"
        },
        "ERPLY Account Production" : {
          "customer code" : "408630",
          "password" : "Farmer5555",
          "username" : "mark@farmraiser.com"
        },
        "ERPLY Account Staging" : {
          "ID" : "262968",
          "password" : "Farmer5555",
          "user" : "mark@farmraiser.com"
        },
        "Linode staging and NEW production" : {
          "password" : "FarmServe123$55",
          "username" : "FRTestbed"
        },
        "NEW production" : {
          "mysql deployer password" : "jT8vgpG%x2W/3H*# ",
          "mysql root password" : "jT8vgpG%x2W/3H*#",
          "password" : "FarmRaiser@123",
          "ssh" : "-p6412 deployer@45.79.218.222"
        },
        "NationBuilder Staging Dev" : {
          "email" : "selsheikh@norglobe.com",
          "password" : "farmraiserdemo",
          "slug" : "farmraisertest",
          "url" : "http://nationbuilder.com/nation_login"
        },
        "Nationuilder Production" : {
          "email" : "ahmed.mostafa@robustastudio.com",
          "password" : "blueteeth",
          "slug" : "farmraiser",
          "url" : "http://nationbuilder.com/nation_login"
        },
        "New staging" : {
          "password" : "FarmRaiser@123",
          "ssh" : "-p6311 deployer@45.79.162.152",
          "user" : "deployer"
        },
        "Server Access" : {
          "IP" : "-p6311",
          "password" : "FarmRaiser@123",
          "user" : "deployer"
        },
        "Stripe production account" : {
          "Email" : "mark@farmraiser.com",
          "Password" : "FarmerBrown321!"
        },
        "linode" : {
          "name" : "frtest",
          "password" : "Farmer123$"
        },
        "redis password" : {
          "command" : "echo -n \"farm-raiser\" | openssl sha1",
          "hash" : "b96721ca6d4e6d2a34ed5c9c850a2aadc76c9452"
        }
      }
    },
    "Filkhedma" : {
      "sections" : {
        "Godaddy access" : {
          "Email" : "omarnramadan@gmail.com",
          "Password" : "Filkhedma2014",
          "User" : "63967684"
        },
        "Linode account1" : {
          "Password" : "Filkhedma2015",
          "User" : "oramadan"
        },
        "Linode account2" : {
          "Passwrod" : "hisham123",
          "User" : "mhisham"
        },
        "Mailgun" : {
          "Password" : "Filkhedma2014",
          "User" : "oramadan@filkhedma.com"
        },
        "Server access" : {
          "Email" : "omarnramadan@gmail.com",
          "command" : "ssh root@176.58.101.151",
          "password" : "Filkhedma2014"
        }
      }
    },
    "GS1" : {
      "sections" : {
        "Dashboard" : {
          "IP" : "46.101.38.225",
          "Password" : "Ahmed@123",
          "URL" : "https://manage.arabhosters.com",
          "User name" : "ahmed.mostafa@robustastudio.com"
        },
        "Production Database" : {
          "Database " : "gs1_production",
          "Password " : "P34kqwaq@A",
          "Username " : "production"
        },
        "Server Access" : {
          "passphrase " : "L#4oqpwe1Aqqe",
          "ssh" : "office@46.101.38.225 -p41937",
          "ssh access " : "ssh -i office_key office@46.101.38.225 -p41937"
        },
        "Staging Database" : {
          "Database " : "gs1_staging",
          "Password " : "P$#5qQWA1F3q",
          "Username " : "staging"
        },
        "admin control panel" : {
          "password " : "SQm%zLuZET4r",
          "url" : "https://46.101.38.225:10000/",
          "username " : "office"
        }
      }
    },
    "Golden Stores" : {
      "sections" : {
        "Dashboard" : {
          "email" : "info@gs.pg.com",
          "password" : "GoldenStores@123",
          "url" : "http://golden-stores.production.rails.robustastudio.com"
        },
        "Server Access" : {
          "password" : "Robusta.123",
          "ssh" : "root@139.162.206.92"
        }
      }
    },
    "InternsGoPro" : {
      "sections" : {
        "Design" : {
          "url" : "https://projects.invisionapp.com/share/3BBUA2B9T#/screens/237836862"
        },
        "Linode" : {
          "Username" : "InternsGoPro",
          "command" : "ssh root@172.104.144.122",
          "password" : "Trusted10+",
          "sudo password" : "InternsGoPro@123"
        },
        "MailChimp" : {
          "email" : "mohamed.hisham@robustastudio.com",
          "password" : "Internsgopro@123",
          "username" : "transparencyatwork"
        },
        "Wireframes" : {
          "url" : "https://projects.invisionapp.com//share/FSBDJK8T4#/screens/229871829"
        }
      }
    },
    "Journey" : {
      "sections" : {
        "Godaddy Account" : {
          "Email" : "adel@journeywithyou.com",
          "Password" : "Adel23101215",
          "Pin" : "2310",
          "Username" : "aelhalwagy"
        },
        "Google Play Developer Account" : {
          "Email" : "adel.jcareer@gmail.com",
          "Password" : "Adel23101215"
        },
        "Linode Account" : {
          "Email" : "adel@journeywithyou.com",
          "Password" : "Adel23101215",
          "Username" : "aelhalwagy"
        },
        "Server Access" : {
          "IP" : "139.162.222.82",
          "mysql password" : "MYSQLR@nd0m",
          "password" : "Jcareer@123 ",
          "username" : "root"
        },
        "Server Configurations" : {
          "IP" : "139.162.222.82",
          "mysql password" : "MYSQLR@ndOm",
          "password" : "Jcareer@123",
          "username" : "deployer"
        }
      }
    },
    "Khoshala" : {
      "sections" : {
        "Admin" : {
          "email" : "admin@khoshala.com",
          "password" : "Khoshala123",
          "url" : "http://www.khoshala.net/admins/sign_in"
        },
        "DNS" : {
          "URL" : "https://onhp.oriented.net/",
          "password" : "khoshala1​234​",
          "username" : "webmaster@khoshala.net"
        },
        "Linode server" : {
          "command" : "ssh -p 6311 deployer@45.79.166.179",
          "pasword" : "Khoshala@123"
        },
        "NEW SERVER" : {
          "command" : "ssh drrivo@dallas110.arvixeshared.com",
          "cpanel" : "https://dallas110.arvixeshared.com:2083/",
          "password" : "Y(2Zhjq)",
          "username" : "drrivo"
        },
        "Server Access" : {
          "command" : "ssh -p 29992 wrr10429@khoshala.net",
          "hostname" : "khoshala.net",
          "password" : "fdPLWg4q0",
          "ssh port number" : "29992",
          "username" : "wrr10429"
        },
        "repo" : {
          "url" : "https://github.com/menisy/khoshala"
        }
      }
    },
    "Limopedia" : {
      "sections" : {
        "Dashboard" : {
          "email" : "info@limopedia.com",
          "password" : "Limopedia@123",
          "url" : "http://manage.limopedia.com/login"
        },
        "Server Access" : {
          "MySQL root password" : "limopedia123",
          "Password" : "limopedia123",
          "Root password" : "limopedia123",
          "URL" : "api.limopedia.com",
          "Username" : "deployer"
        }
      }
    },
    "MallBuddy" : {
      "sections" : {
        "Godaddy & Linode" : {
          "Godaddy PIN" : "9753",
          "email" : "msharaf@identityeg.com",
          "password" : "M@llBuddy2017",
          "username" : "mallbuddcMallbuddy1"
        },
        "Linode machine " : {
          "deployer password" : "5923gsdir#@",
          "machine" : "root@139.162.146.95",
          "root mysql" : "fate2tayni8498$123",
          "root password" : "3$gotcat#124"
        }
      }
    },
    "Mirqah" : {
      "sections" : {
        "Linode Account credentials" : {
          "password" : "mir444qah",
          "user" : "mirqah"
        },
        "Server Credetials" : {
          "IP" : "139.162.227.242",
          "Password" : "Mirqah@123",
          "User" : "deployer"
        }
      }
    },
    "P&G" : {
      "sections" : {
        "Server Access" : {
          "Deployer password" : "41223Bsdf238%2",
          "IP" : "139.162.206.92",
          "Mysql root password" : "435@3fv@5234",
          "root password" : "9Kp3y2k76zWvLhuh"
        }
      }
    },
    "PassApp" : {
      "sections" : {
        "Dashboard" : {
          "Password" : "^pSq}lE2xPgG",
          "URL" : "https://257422731120.signin.aws.amazon.com/console",
          "Username" : "robusta_studio"
        },
        "New Relic Account (Analytics and Performance Monitoring)" : {
          "Password" : "9t7K;4XACF?A,?+9",
          "Username" : "info@passapp.com"
        },
        "PassApp-io login" : {
          "Email" : "admin@passapp.io",
          "Password" : "BWe-6QW%/W4w\\.5S"
        },
        "RDS" : {
          "Database ID" : "passapp-production",
          "Database name" : "passapp_production",
          "Password" : "a4a287ddabb532b02d0",
          "Port" : "5432",
          "User name" : "postgres",
          "endpoint" : "passapp-production.ccov76ksmdpd.us-west-2.rds.amazonaws.com"
        },
        "bitly" : {
          "Password" : "Q:t_4P}r;/@Z25p",
          "Username" : "info@passapp.io"
        },
        "email" : {
          "password" : "worldofwarcraft",
          "user" : "passappdeployer@roubustastudio.com"
        },
        "production server " : {
          "dbpassword" : "10385f7d503ba6aeef34055e",
          "dbusername" : "postgres"
        }
      }
    },
    "Peeps" : {
      "sections" : {
        "Linode" : {
          "Password" : "Insomnia(1",
          "Username" : "mohammadyahfoufi"
        },
        "host" : {
          "command" : "ssh root@45.79.172.36",
          "command2" : "ssh -p 6311 deployer@45.79.172.36",
          "password" : "Peeps@123",
          "root db password" : "Peeps@123"
        }
      }
    },
    "Pinmelocal" : {
      "sections" : {
        "NEW SERVER Control Panel" : {
          "Password" : "G_&6zU@[p2Dq",
          "URL" : "http://74.208.66.39:2082",
          "Username" : "pinmelocal"
        },
        "NEW SERVER Database Details" : {
          "Database name" : "pinmeloc_pml",
          "Password" : "%i%EXxOn@$Wq",
          "Username" : "pinmeloc_pml"
        },
        "NEW SERVER FTP Details" : {
          "FTP Host" : "74.208.66.39",
          "FTP Password" : "G_&6zU@[p2Dq",
          "FTP Port" : "21",
          "FTP Username" : "pinmelocal "
        },
        "NEW SERVER SSH Details" : {
          "Password" : "G_&6zU@[p2Dq",
          "SSH IP" : "74.208.66.39",
          "SSH Port" : "22568",
          "SSH User" : "pinmelocal"
        },
        "ON OLD SERVER Control Panel" : {
          "Password" : "5PmJnhHo6VI3",
          "URL" : "https://72.55.164.89:8443",
          "Username" : "admin "
        },
        "ON OLD SERVER Database Details" : {
          "Database Name" : "norglobe_pml",
          "Database User Password" : "pass@word",
          "Database Username" : "pml"
        },
        "ON OLD SERVER FTP Details" : {
          "FTP Host" : "72.55.132.28",
          "FTP Password" : "&6zU@[p2",
          "FTP Port" : "21",
          "FTP Username" : "pinmelocal "
        },
        "ON OLD SERVER SSH Details" : {
          "Password" : "5PmJnhHo6VI3",
          "SSH IP" : "72.55.164.89",
          "SSH Port" : "22023",
          "Ssh User" : "root"
        },
        "SSH ROOT Details" : {
          "Password" : "TpIKVvys&)(%BuAS5kq3oBA",
          "SSH IP address" : "74.208.66.39",
          "SSH Port" : "22568",
          "SSH User" : "root"
        },
        "Transloadit Details" : {
          "password" : "Thecell1??",
          "user" : "selsheikh@pinmelocal.com"
        }
      }
    },
    "Rails Staging" : {
      "sections" : {
        "Server Access" : {
          "host" : "178.79.158.14",
          "password" : "RobustaRails@123",
          "port" : "6311",
          "ssh" : "-p6311 deployer@178.79.158.14",
          "user" : "deployer"
        }
      }
    },
    "RecForFree" : {
      "sections" : {
        "Godaddy" : {
          "password" : "Qilatadab@86",
          "user" : "87719363"
        },
        "Linode Account" : {
          "URL" : "http://www.linode.com",
          "new password" : "&hNmvMWB@WMw",
          "username" : "ahmedmostafa343"
        },
        "SendGrid" : {
          "URL" : "https://sendgrid.com",
          "password" : "Khateera@86",
          "username" : "gamil86"
        },
        "Server Access" : {
          "password" : "emad.123",
          "ssh" : "root@212.71.234.223"
        }
      }
    },
    "Robusta Sharing Centre" : {
      "sections" : {
        "Creative team" : {
          "Password" : "creative",
          "Username" : "crteam"
        },
        "Developer team" : {
          "Password" : "developer",
          "Username" : "devteam"
        },
        "Operations team" : {
          "Password" : "operations",
          "Username" : "opsteam"
        }
      }
    },
    "SeaShell" : {
      "sections" : {
        "Database Access" : {
          "database" : "seashell_production",
          "password" : "tR53$534D352&",
          "user" : "emad"
        },
        "Seashell Credentials for AWS & Twilio are" : {
          "Email" : "info@robustastudio.com",
          "password" : "Robusta@123"
        },
        "Server Access" : {
          "SSH port " : "22",
          "Server ip" : "139.162.216.124",
          "Username" : "emad"
        }
      }
    },
    "Robusta Services" : {
      "sections" : {
        "GoRails" : {
          "password" : "Robusta@123",
          "user" : "rails@robustastudio.com"
        },
        "Helpdesk" : {
          "password" : "your$oul$hallb9",
          "url" : "http://helpdesk.robustastudio.com/"
        },
        "Mandrill credentials" : {
          "Password" : "MobileAppsRobusta.",
          "Username" : "mobileapps@robustastudio.com",
          "url" : "https://mandrillapp.com"
        },
        "New Postman" : {
          "password" : "Robusta.123",
          "user" : "pmo@robustastudio.com"
        },
        "Postman" : {
          "password" : "Robusta.123",
          "user" : "rails@robustastudio.com"
        },
        "Robusta AWS Mobile Team" : {
          "email" : "mobileapps@robustastudio.com",
          "password" : "MobileAppsRobusta"
        },
        "Robusta AWS Old Galal Account" : {
          "email" : "galal.aly@robustastudio.com",
          "password" : "Robusta.123"
        },
        "Robusta AWS Rails Team" : {
          "email" : "rails@robustastudio.com",
          "password" : "RobustaRails@123"
        },
        "Twilio" : {
          "email" : "islam.abdelraouf@robustastudio.com",
          "password" : "MobileAppsRobusta."
        }
      }
    },
    "Snap Grad" : {
      "sections" : {
        "Server Credetials" : {
          "mysql password" : "SnapGrad@123",
          "password" : "SnapGrad@123",
          "ssh" : "deployer@45.79.99.239"
        },
        "misc" : {
          "Tree digram" : "https://drive.google.com/a/robustastudio.com/file/d/0BxjYKHAi0h11azBRQVdlM2FiYzQ/view?usp=sharing"
        }
      }
    }
  },
  "roles" : {
    "admin" : {
      "838473129463971426" : true
    }
  },
  "users" : {
    "489q798934HY943H" : {
      "email" : "dsfk@gmail.com",
      "uid" : "489q798934HY943H"
    }
  }
}
