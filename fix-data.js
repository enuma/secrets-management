const fs = require('fs');
const data = require('./data');


let shallowProjects = data.projects_names;
let projectsData = data.projects;
 
let modifiedData = {};

function normalize(str = '') {
  return str.replace(/[\\/+*&_-]/g, ' ').replace(/\s+/g, '-').toLowerCase();
}

let projectsNames = Object.keys(shallowProjects);
// let normalizedProjectsNames = Object.keys(shallowProjects).map(normalize);


modifiedData['projects_names'] = projectsNames.reduce((acc, key) => {
  let normalizedKey = normalize(key);
  acc[normalizedKey] = {
    name: key,
    logo: shallowProjects[key].logo,
    sections: null
  };
  return acc;
}, {});


modifiedData['projects'] = projectsNames.reduce((acc, key) => {
  let normalizedKey = normalize(key);
  let project = projectsData[key];
  project.name = key;
  project.logo = shallowProjects[key].logo;
  acc[normalizedKey] = project;
  return acc;
}, {});


fs.writeFileSync('modified-data.json', JSON.stringify(modifiedData, null, 2));
