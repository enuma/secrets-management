import React, {Component} from 'react';
import {Route, withRouter} from 'react-router';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';

import * as actionCreators from '../actions/actionCreators';
import StoreWrapper from './../StoreWrapper';

import Project from './Project';
import ProjectsGrid from './ProjectsGrid';
import Navbar from './Navbar';
import Loading from './Loading';

import firebase from '../firebase';

const provider = new firebase.auth.GoogleAuthProvider();
const auth = firebase.auth();

class App extends Component {
  constructor() {
    super();
    this.login = this.login.bind(this);
    this.logout = this.logout.bind(this);
  }
  
  componentDidMount() {
    auth.onAuthStateChanged((user, error, completed) => {
      if (error) {
        console.error('Error Auth', error);
      }
      if (user) {
        StoreWrapper.authenticateUser(user);
        StoreWrapper.fetchProjects();
      }
    });
  }

  componentWillMount() {
    console.log('App will mount');
    let authUser = StoreWrapper.getState().authUser;
    // debugger;
    if (authUser) {
      StoreWrapper.fetchProjects();
    }
  }
  
  login() {
    return auth.signInWithPopup(provider)
    .then(result => {
      // if (result && result.user) {
      //   const user = result.user;
      //   StoreWrapper.authenticateUser(user);
      //   StoreWrapper.fetchProjects();
      // }
    }).catch(err => console.log('Error logging in', err) || localStorage.clear());
  }

  logout() {

  }
  
  componentWillUpdate() {
    console.log('App will update');
  }

  render() {
    let authUser  = StoreWrapper.getState().authUser;
    let userIsLoggedIn = !!Object.keys(localStorage).find(i => i.startsWith('firebase:authUser'));
    let dataInitialization = StoreWrapper.getState().dataInitialization;
    if (dataInitialization.error && dataInitialization.error.code === 'PERMISSION_DENIED') {
      localStorage.clear();
    }
    if (userIsLoggedIn && !authUser) {
      return (
        <Loading text="Checking if you're already logged in . . ."/>
      );
    } else if (!authUser) {
      return (
        <div className="login-form">
          <button type="button" className="login-btn btn btn-primary btn-lg btn-block" onClick={this.login}>Login</button>
          <p className="help-message">Please, login with you robusta email</p>
        </div>
      );
    } else  if (dataInitialization.state) {
      if (dataInitialization.error) {
        return (<p>Error! {JSON.stringify(dataInitialization.error)}</p>)
      } else {
        return (
          <div>
            <Navbar login={this.login} logout={this.logout}/>
            <div className="container">
              <switch>
                <Route exact path="/" component={ProjectsGrid}/>
                <Route path="/projects/:projectId" component={Project}/>
              </switch>
            </div>
          </div>
        );
      }
    } else {
      return (
        <Loading text="You're already logged in, Loading data now . . ." />
      )
    }
  }
}
function mapStateToProps(state) {
  return { ...state };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(actionCreators, dispatch);
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
