import React from 'react';
import {Link} from 'react-router-dom';
// import CSSTransitionGroup from 'react-addons-css-transition-group';

import StoreWrapper from '../StoreWrapper';

import Section from './Section';
import Loading from './Loading';

class Project extends React.Component {

  componentWillMount() {
    this.projectId = decodeURIComponent(this.props.match.params.projectId);
    this.projectPath = `/projects/${encodeURIComponent(this.projectId)}`;
    this.projectEditPath = `${this.projectPath}/edit`;
    let project = this.getProject();
    if (project && !project.sections) {
      StoreWrapper.fetchProject(this.projectId);
    }
  }

  componentWillUpdate(newProps){
    // debugger;
    let path = newProps.location.pathname;
    this.isProjectEdit = path.lastIndexOf(this.projectEditPath) === 0;
    let sectionEditTest = path.match(new RegExp('^/.*/sections/(.*)/edit$')); 
    this.editSectionName = sectionEditTest && sectionEditTest[1];
    this.editSectionName = this.editSectionName && decodeURIComponent(this.editSectionName);
  }

  getProject() {
    return this.props.project || StoreWrapper.getState().projects[this.projectId];
  }

  render() {
    let project = this.getProject(),
      {isProjectEdit, projectPath, editSectionName} = this;
      // debugger
      let sections = project.sections,
      projectName = project.name,
      projectLogo = project.logo;

    if (!sections) {
      return (
        <div className="projecT">
          <Loading text="Loading Project Data . . ."/>
        </div>
      );
    } else {
      return (
        <div className="project" >
          <div className="project__header">
            <Link className="back-btn" to={isProjectEdit? projectPath : '/'}>
              <span className="fa fa-arrow-circle-left"></span>
            </Link>
            
            {!isProjectEdit &&
              <h2 className="project__title">
                <img className="project__logo" src={projectLogo} alt={projectName} />
                <span>{projectName}</span>
              </h2>
            }
            {isProjectEdit && 
              <form className="project__title">
                <div className="col-12 col-sm-10 offset-sm-1">
                  <input className="form-control" type="text" value={projectName} />
                </div>
              </form>
            }
            {/* { !isProjectEdit &&
              <Link className="project__edit" to={projectEditPath}>
                <span className="fa fa-edit"></span>
              </Link>
            } */}
            {/* { isProjectEdit &&
              <Link className="project__edit" to={projectPath}>
                <span className="fa fa-save"></span>
              </Link>
            } */}
          </div>
          <div className="row sections-list">
            {
              Object.keys(sections).map(sectionTitle => (
                <div key={sectionTitle} className="col-md-6 project__section-wrapper">
                  <Section title={sectionTitle} isEdit={sectionTitle === editSectionName} projectPath={projectPath} section={sections[sectionTitle]} />
                </div>
              ))
            }
          </div>
        </div>
      );
    }
  }
}

export default Project;
