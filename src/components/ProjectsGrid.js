import React from 'react';
import {Link} from 'react-router-dom';
import StoreWrapper from '../StoreWrapper';
import lcs from 'longest-common-subsequence';

class ProjectsGrid extends React.Component {

  componentWillUpdate() {
    console.log('projectsGrid component will update');
  }

  getProjects() {
    let { projects, filters } = StoreWrapper.getState();
    let projectsList = Object.keys(projects).map(key => {
      return {
        ...projects[key],
        id: key,
      };
    });

    if (filters.search) {
      for (let project of projectsList) {
        let lcsResult = lcs(project.name, filters.search) || '';
        project.noView = lcsResult.length !== filters.search.length ;
      }
    }
    return projectsList;
  }

  render() {
    let projects = this.getProjects();
    return (
      <div>
        <h2 className="projects-title">Projects List</h2>
        <div className="row projects-list">
          {
            projects.map(project => {
              if (project.noView) {
                return null;
              }
              return (
              <div className="col-md-4 col-sm-6 project" key={project.id} >
                <Link to={'/projects/' + encodeURIComponent(project.id)} className="project--grid">
                  <div className="project__logo">
                    <img src={project.logo} alt="project logo" />
                  </div>
                  <h3 className="project__title">
                    {project.name}
                  </h3>
                </Link>
              </div>
            )
          })
          }
        </div>
      </div>
    )
  }
}

export default ProjectsGrid;
