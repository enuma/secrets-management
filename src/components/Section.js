import React from 'react';
import {Link} from 'react-router-dom';

class Section extends React.Component {
  getData() {
    let isEdit = this.props.isEdit;
    let sectionTitle =null;
    let section =null;
    let projectPath =null;
    let sectionEditPath =null;
    // let projectId = null;
    sectionTitle = this.props.title;
    section = this.props.section;
    projectPath = this.props.projectPath;
    sectionEditPath = `${projectPath}/sections/${encodeURIComponent(sectionTitle)}/edit`
    return {
      isEdit,
      sectionTitle,
      section,
      projectPath,
      sectionEditPath,
    };
  }
  render() {
    let {isEdit, sectionTitle, section, projectPath} = this.getData();
    return (
      <div className="project__section section">
        <div className="project__header">
          { isEdit &&
              <Link className="back-btn" to={projectPath}>
                <span className="fa fa-arrow-circle-left"></span>
              </Link>
          }
          <h2 className="section__title">
            { !isEdit && sectionTitle }
            { isEdit &&
              <div className="col-8 offset-2 col-sm-6 offset-sm-3">
                <input className="form-control" type="text" value={sectionTitle} />
              </div>
            }
            
          </h2>
          {/* { !isEdit &&
            <Link className="project__edit" to={sectionEditPath}>
              <span className="fa fa-edit"></span>
            </Link>
          } */}
          {/* { isEdit &&
            <Link className="project__edit" to={projectPath}>
              <span className="fa fa-save"></span>
            </Link>
          } */}
        </div>
        <div className="section__content-wrapper">
          {
            Object.keys(section).map(sectionContentKey => (
              <div className="section__content form-group row" key={sectionContentKey}>
                <label className="section__label col-4 col-form-label" htmlFor={sectionTitle + '-' + sectionContentKey}>{sectionContentKey}</label>
                <div className="col-8">
                  <input className="section_input form-control" id={sectionTitle + '-' + sectionContentKey} value={section[sectionContentKey]} readOnly/>
                </div>
              </div>
            ))
          }
        </div>
      </div>
    )
  }
}

export default Section;
