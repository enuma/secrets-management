import React from 'react';
import _ from 'underscore';

import StoreWrapper from '../StoreWrapper';

class Navbar extends React.Component {
  handleKeyPress() {
    return _.debounce((input) => {
      StoreWrapper.filterProjects({search: input.value});
    }, 500)
    
  }
  render() {
    let keyPressHandler = this.handleKeyPress();
    return (
      <header>
        <nav className="navbar navbar-toggleable-md navbar-light">
          <form className="form-search form-inline my-2 my-lg-0">
            <input className="form-control col-sm-4 offset-sm-4 mr-sm-6" type="text" placeholder="Search" onKeyPress={ev => {
              keyPressHandler(ev.target);
            }}/>
            {/* <button className="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button> */}
          </form>
        </nav>
      </header>
    );
  }
}

export default Navbar;
