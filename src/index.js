import React from 'react';
import {render} from 'react-dom';
import {Router} from 'react-router';
import {Provider} from 'react-redux';

// Import Components
import App from './components/App';
import registerServiceWorker from './registerServiceWorker';
import StoreWrapper, {history} from './StoreWrapper';

import './css/bootstrap.min.css';
import './css/font-awesome.min.css';
import './css/style.css';

// StoreWrapper.fetchProjects();

const router = (
  <Provider store={StoreWrapper.store}>
    <Router history={history}>
      <div>
        <App />
      </div>
    </Router>
  </Provider>
)

render(router, document.getElementById('root'));

registerServiceWorker();
