import { routerMiddleware } from "react-router-redux";
import { createBrowserHistory } from 'history';
import { createStore, applyMiddleware, compose, bindActionCreators } from "redux";

import thunk from "redux-thunk";
// import { composeWithDevTools } from 'redux-devtools-extension';

import * as actionCreators from './actions/actionCreators';
// reducer
import rootReducer from './reducers/index';



// create an object for the default data
const defaultState = {
  projects: null,
  users: null,
  dataInitialization: { 
    state: false,
    error: null,
  },
  authUser: null,
  filters: {
    
  }
};


// export const history = syncHistoryWithStore(createBrowserHistory(), store);
export const history = createBrowserHistory();
history.listen((location, action) => {
  console.log(`The current URL is ${location.pathname}${location.search}${location.hash}`)
  console.log(`The last navigation action was ${action}`)
})

const middleware = routerMiddleware(history);

const store = createStore(rootReducer, defaultState, (window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose)(applyMiddleware(middleware, thunk)));

if (module.hot) {
  module
    .hot
    .accept('./reducers/', () => {
      const nextRootReducer = require('./reducers/index').default;
      store.replaceReducer(nextRootReducer);
    });
}

const boundActionCreators = bindActionCreators(actionCreators, store.dispatch);

export default {
  store,
  getState: store.getState,
  ...boundActionCreators
}
