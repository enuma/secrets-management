import ACTION_TYPES from './constants';
import firebase from 'firebase';
// import {database as firebaseDB} from '../firebase';

// const projectsRef = firebaseDB.ref('/projects');

// increment
export function fetchProjects() {
  console.log('fetch projects call');
  return dispatch => firebase.auth().currentUser.getIdToken(true)
    .then(idToken => fetch(`https://us-central1-robusta-secrets-management.cloudfunctions.net/listProjects?idToken=${encodeURIComponent(idToken)}`))
    .then(res => res.json())
    .then(projects => dispatch({
      type: ACTION_TYPES.FETCH_PROJECTS,
      payload: projects,
      error: null
    }))
    .catch(err => {
      return dispatch({
        type: ACTION_TYPES.FETCH_PROJECTS,
        payload: {},
        error: err,
      });
    });
}

export function filterProjects(filters) {
  return {
    type: ACTION_TYPES.FILTER_PROJECTS,
    filters,
  }
}

export function authenticateUser(user = null, error = null) {
  return {
    type: ACTION_TYPES.AUTHENTICATE_USER,
    user,
    error
  }
}

export function fetchProject(projectId) {
  return dispatch => firebase.auth().currentUser.getIdToken()
    .then(idToken => fetch(`https://us-central1-robusta-secrets-management.cloudfunctions.net/getProject?name=${encodeURIComponent(projectId)}&idToken=${encodeURIComponent(idToken)}`))
    .then(res => res.json())
    .then(projectDetails => dispatch({
      type: ACTION_TYPES.FETCH_PROJECT_DETAILS,
      payload: {
        project: projectDetails,
        id: projectId,
      },
      error: null,
    }))
    .catch(err => dispatch({
      type: ACTION_TYPES.FETCH_PROJECT_DETAILS,
      payload: {},
      error: err,
    }));
}
