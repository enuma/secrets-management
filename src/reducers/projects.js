import ACTION_TYPES from '../actions/constants';

function loadProjects(state={}, action) {
  return { ...action.payload };
}

function loadProject(state={}, action) {
  // debugger;
  let newState = {...state};
  newState[action.payload.id] = { ...action.payload.project };
  return newState;
}

function projects(state = {}, action) {
  console.log('projects reducer', action);
  switch (action.type) {
    case ACTION_TYPES.FETCH_PROJECTS:
      return loadProjects(state, action);
    case ACTION_TYPES.FETCH_PROJECT_DETAILS:
      return loadProject(state, action);
    default: return state;
  }
}

export default projects;
