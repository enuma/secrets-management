import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import ACTION_TYPES from '../actions/constants'
import users from './users';
import projects from './projects';

function dataInitialization(state = {}, action) {
  if (action.type === ACTION_TYPES.FETCH_PROJECTS) {
    return {
      state: true,
      error: action.error
    };
  }
  return state;
}

function authUser(state = null, action) {
  if (action.type === ACTION_TYPES.AUTHENTICATE_USER) {
    let newState = null;
    if (action.user) {
      newState = {...action.user};
    }
    if (action.error) {
      newState.error = action.error;
    }
    return newState;
  }
  return state;
}

function filters(state={}, action) {
  if (action.type === ACTION_TYPES.FILTER_PROJECTS) {
    let newState = { ...state };
    if (action.filters.search) {
      newState.search = action.filters.search
    }
    return newState;
  }
  return state;
}
const rootReducer = combineReducers({users, projects, dataInitialization, authUser, filters, routing: routerReducer });

export default rootReducer;
