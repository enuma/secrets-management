import * as firebase from 'firebase';
const config = {
  apiKey: "AIzaSyBP-gpE8qSn3NsdWOBHrX2CeT6CFGFvjgg",
  authDomain: "robusta-secrets-management.firebaseapp.com",
  databaseURL: "https://robusta-secrets-management.firebaseio.com",
  projectId: "robusta-secrets-management",
  storageBucket: "robusta-secrets-management.appspot.com",
  messagingSenderId: "446983374353"
};
const defaultApp = firebase.initializeApp(config);
export const database = defaultApp.database();
export default firebase;
